#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int thread_count;
int m,n;
double **A, *x, *y;

void* Pth_math_vect(void* rank){
  long my_rank = (long) rank;
  //printf("%d rank\n", my_rank);
  int local_m = m/thread_count;
  int my_first_row = my_rank * local_m;
  int my_last_row = (my_rank+1) * local_m - 1;
  for(int i = my_first_row; i <= my_last_row; i++){
    y[i] = 0.0;
    for(int j = 0; j < n; j++){
      y[i] += A[i][j] * x[j];
    }
  }
  return NULL;
}


int main(int argc, char* argv[]){
  scanf("%d %d", &m, &n);
  A = (double**) malloc(m*sizeof(double));
  x = (double*) malloc(n*sizeof(double));
  y = (double*) malloc(m*sizeof(double));
  for(int i = 0; i < m; i++){
    A[i] = (double*) malloc(n*sizeof(double));
    for(int j = 0; j < n; j++){
      A[i][j] = 1;
    }
  }
  for(int i = 0; i < m; i++) { y[i] = 0; }
  for(int i = 0; i < n; i++) { x[i] = 2; }
  thread_count = strtol(argv[1],NULL,10);
  pthread_t*  thread_handles = malloc(thread_count*sizeof(pthread_t));  
  for(long thread = 0; thread < thread_count; thread++) {    
    pthread_create(&thread_handles[thread], NULL, Pth_math_vect, (void*) thread);
  }
  for(long thread = 0; thread < thread_count; thread++) {
    pthread_join(thread_handles[thread],NULL);
  }
  
  for(int i = 0; i < m;i++)
    printf("%f ",y[i]);
    printf("\n");
  free(thread_handles);
  return 0;
}