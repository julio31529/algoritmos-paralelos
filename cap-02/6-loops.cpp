#include <iostream>
#include <stdio.h>
#include <time.h>

using namespace std;

#define MAX 10

int main() {
	int tam = MAX/10;
	int **a = new int * [MAX];
	int **b = new int * [MAX];
	int ** resultado = new int * [MAX];

	for(int i=0; i<MAX; i++) {
			a[i]=new int [MAX];
			b[i]=new int [MAX];
			resultado[i]=new int [MAX];
			for(int j=0; j<MAX; j++) {
				a[i][j]=rand()%1000;
				b[i][j]=rand()%1000;
				resultado[i][j]=0;
			}
	}
	clock_t inicio, fin;
	double tiempoTotal;

	inicio = clock();
	for(int x=0; x<MAX; x+=tam){
		for(int y=0; y<MAX; y+=tam){
			for(int z=0; z<MAX; z+=tam){
				for(int i=x; i<x+tam; i++){
					for(int j=y; j<y+tam; j++){
						for(int k=z; k<z+tam; k++){
							resultado[i][j]+=a[i][k]*b[k][j];
						}
					}
				}
			}
		}
	}
	fin = clock();
	tiempoTotal = ((double) (fin - inicio)) * 1000 / CLOCKS_PER_SEC;  
	cout<< "Tiempo : " << tiempoTotal << endl;
}