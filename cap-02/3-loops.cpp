#include <iostream>
#include <stdio.h>
#include <time.h>

using namespace std;

#define MAX 100

void alg_3_loops() {
		int **a = new int * [MAX];
	int **b = new int * [MAX];
	int **c = new int * [MAX];
	for(int i=0; i<MAX; i++) {
		a[i]=new int [MAX];
		b[i]=new int [MAX];
		c[i]=new int [MAX];
		for(int j=0; j<MAX; j++) {
			a[i][j]=rand()%1000;
			b[i][j]=rand()%1000;
			c[i][j]=0;
		}
	}
	clock_t inicio, fin;
	double tiempoTotal;

	inicio = clock();
	for (int i=0; i<MAX; i++) {
		for (int j=0; j<MAX; j++) {
			c[i][j] = 0;
			for (int k=0; k<MAX; k++) {
				c[i][j] += a[i][k] * b[k][j];
			}
		}
	}  
	fin = clock();
	tiempoTotal = ((double) (fin - inicio)) * 1000 / CLOCKS_PER_SEC;  
	cout<< "Tiempo : " << tiempoTotal << endl;
}

int main() {
	alg_3_loops();
	return 0;
}
